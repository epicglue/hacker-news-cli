package main

import "gitlab.com/epicglue/hacker-news-cli/cmd"

func main() {
	cmd.Execute()
}
